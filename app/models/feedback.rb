# frozen_string_literal: true

class Feedback < ActiveRecord::Base
  validates :comment, presence: true

  belongs_to :user
  belongs_to :post

  # create feedback for post or user
  def self.create_feedback(comment: nil,owner_id: nil, user_id: nil, post_id: nil)
    feedback = Feedback.create!(comment: comment, owner_id: owner_id, user_id: user_id, post_id: post_id)
    { data: feedback, status: 200, message: 'Feedback created successfully!' }
  end

  # get all the feedback for user and post
  def feedback_for_posts_and_users
    all_feedback = Feedback.all
    { data: all_feedback}
  end
end
