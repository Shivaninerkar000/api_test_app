# frozen_string_literal: true
require 'json'
class Post < ActiveRecord::Base
  validates :title, :content, presence: true

  belongs_to :users
  has_many :ratings

  # create new post
  def self.create_post(title: nil, content: nil)
    post = Post.find_by(title: title, content: content)
    if post.present?
      { data: post, status: 200, message: 'Post exits.........' }
    else
      post = Post.create!(title: title, content: content)
      { data: post, status: 200, message: 'Post create successfully!' }
    end
  end

  # get all post
  def all_posts
    all_post = Post.all
    { data: all_post}
  end
end