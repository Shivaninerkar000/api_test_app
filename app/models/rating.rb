# frozen_string_literal: true
require 'json'

class Rating < ActiveRecord::Base
  validates :rate, presence: true, inclusion: { in: 1..5 }
  belongs_to :posts

  # create rating for the post
  def self.create_rating(rate: nil, post_id: nil, user_id: nil)
    rating = Rating.create!(rate: rate, post_id: post_id, user_id: user_id)
    { data: rating, status: 200, message: 'Rate provided successfully!' }
  end

  # get all ratings for post
  def rating_for_posts
    all_rating = Rating.all
    { data: all_rating}
  end
end
