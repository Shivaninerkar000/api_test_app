# frozen_string_literal: true

require 'json'
class User < ActiveRecord::Base
  validates :username, presence: true, uniqueness: true
  validates :password, presence: true

  has_many :posts

  def self.authentication(username: nil, password: nil)
    @user = User.find_by(username: username, password: password)
    if @user.present?
      { data: @user, status: 200, message: 'Login Successfull...' }
    else
      @user = User.create!(username: username, password: password)
      { data: @user, status: 200, message: 'User create successfully!' }
    end
  end

  def self.all_users
    all_user = User.all
    { data: all_user}
  end
end
