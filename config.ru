require 'roda'
require './app/main'

class App < Roda
  route do |r|
    # GET /login request
    r.root do 
      r.redirect "/user"
      authenticate
    end
    
    # create new user
    r.post 'user' do
      username = r.params['username']
      puts "#{username}"
      password = r.params['password']
      puts "#{password}"
      authenticate = User.authentication(username: username, password: password)
      authenticate.to_json
    end

    # Login user
    r.get 'users' do 
      all_users = User.all
      all_users.to_json
    end

    # create post
    r.post 'post' do
      title = r.params['title']
      puts "#{title}"
      content = r.params['content']
      puts "#{content}"
      create_post = Post.create_post(title: title, content: content)
      create_post.to_json
    end

    # get all posts
    r.get 'posts' do
      all_posts = Post.all
      all_posts.to_json
    end

    # rate for post/user
    r.post 'rate_of_post' do
      rate = r.params['rate']
      puts "#{rate}"
      post_id = r.params['post_id']
      puts "#{rate}"
      user_id = r.params['user_id']
      puts "#{rate}"
      create_rating = Rating.create_rating(rate: rate, post_id: post_id, user_id: user_id)
      create_rating.to_json
    end

    # get all rating for posts or users
    r.get 'rating' do
      all_rating = Rating.all
      all_rating.to_json
    end

    # create feedback for user or post
    r.post 'feedback' do
      comment = r.params['comment']
      puts "#{comment}"
      owner_id = r.params['owner_id']
      puts "#{owner_id}"
      post_id = r.params['post_id']
      puts "#{post_id}"
      user_id = r.params['user_id']
      puts "#{user_id}"
      create_feedback = Feedback.create_feedback(comment: comment, owner_id: owner_id, post_id: post_id, user_id: user_id)
      create_feedback.to_json
    end

    # get all feedback for post or user
    r.get 'feedbacks' do
      all_feedback = Feedback.all
      all_feedback.to_json
    end
  end
end

run App.freeze.app

